import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Menu, Segment, Icon} from 'semantic-ui-react';
import { push } from 'react-router-redux';
import store from '../../../redux/store';

import './index.css';

export default class FullNavBar extends Component {
  constructor(props) {
    super(props);
      this.state = { activeItem: 'DASHBOARD' }
      this.handleItemClick = this.handleItemClick.bind(this);
  }
 

  handleItemClick(e, { name, link }) {
    this.setState({ activeItem: name });
    store.dispatch(push(link));
  }

  render() {
    const { activeItem } = this.state

    return (
      <div className="nav-bar">
        <Menu>
          <Menu.Item name='DASHBOARD' active={activeItem === 'DASHBOARD'} onClick={this.handleItemClick} link='/'>
            <Icon name='dashboard' /> <span>DASHBOARD</span>
          </Menu.Item>
          <Menu.Item name='CLASS' active={activeItem === 'CLASS'} onClick={this.handleItemClick} link='class'>
            <Icon name='envira' /> <span>Class</span>
          </Menu.Item>
          <Menu.Menu position='right'>
            <Menu.Item name='logout' active={activeItem === 'logout'} onClick={ this.handleItemClick}/>
          </Menu.Menu>
        </Menu>
        {this.props.children}
      </div>
    )
  }
}