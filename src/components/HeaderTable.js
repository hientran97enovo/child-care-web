import React from 'react'
import { Icon, Label, Menu, Table } from 'semantic-ui-react'

const HeaderTable = (props) => {      
    return(
        <Table.Header>
            <Table.Row >
                { props.headerTable.map((item, i)=> {
                    return (
                        <Table.HeaderCell>{item}</Table.HeaderCell> 
                    );
                })}
            </Table.Row>
        </Table.Header>
    );
}

export default HeaderTable;