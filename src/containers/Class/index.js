import React, { Component } from 'react';
import './index.css';
import ClassActions from '../../redux/ClassRedux/actions';
import HeaderTable from '../../components/HeaderTable';
import FullNavBar from '../../components/common/FullNavBar';
import { Icon, Label, Menu, Table } from 'semantic-ui-react';
import { connect } from 'react-redux';

class Class extends Component{
  constructor(props) {
    super(props);
      this.state = {
        classes: []
      }
  }

  componentWillMount(){
    this.props.getClassData();
  }
  componentWillReceiveProps(nextProps) {
    if(!nextProps.loading) {
        this.setState(
        {classes: nextProps.classData}
      )
    }
  }

  renderHeaderTable() {
    const headerTableList= [
      'Class Name', 'Header Teachers', 
      'Code', 'School Year',
      'Description', 'Capacity', 'Teachers' , 'Students'];
    return (
      <HeaderTable headerTable={headerTableList} />
    );
  }

  mapData() {
    if(!this.props.loading){
        return(
          <Table.Body>
          {
            this.state.classes.map((item, i)=> {
            return(
              <Table.Row >
                <Table.Cell>{item.ClassName}</Table.Cell>
                <Table.Cell>{item.headTeacher ? item.headTeacher.name : 'No teacher'}</Table.Cell>
                <Table.Cell>{item.ClassCode}</Table.Cell>
                <Table.Cell>{item.SchoolYear}</Table.Cell>
                <Table.Cell>{item.Description}</Table.Cell>
                <Table.Cell>{item.Capacity}</Table.Cell>
                <Table.Cell>{item.Capacity}</Table.Cell>
                <Table.Cell>{item.ClassName}</Table.Cell>   
              </Table.Row>
            );
          })
        }
        </Table.Body>
        );  
    }
  }
  renderTable() {
    
    return (
      <Table celled>
        {this.renderHeaderTable()}
        {this.mapData()}
 
      </Table>
    )
  }

  renderTableFooter() {
    <Table.Footer>
      <Table.Row>
        <Table.HeaderCell colSpan='3'>
          <Menu floated='right' pagination>
            <Menu.Item as='a' icon>
              <Icon name='left chevron' />
            </Menu.Item>
            <Menu.Item as='a'>1</Menu.Item>
            <Menu.Item as='a'>2</Menu.Item>
            <Menu.Item as='a'>3</Menu.Item>
            <Menu.Item as='a'>4</Menu.Item>
            <Menu.Item as='a' icon>
              <Icon name='right chevron' />
            </Menu.Item>
          </Menu>
        </Table.HeaderCell>
      </Table.Row>
    </Table.Footer>
  }

  render() {

      return (
      <FullNavBar>
        {this.renderTable()}
      </FullNavBar>
      );
  }

}

function mapStateToProps(state) {
  return {
    classData: state.class.data,
    loading: state.class.loading,
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    getClassData: () => dispatch(ClassActions.getClassData()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Class);