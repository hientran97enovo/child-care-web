import React, { Component } from 'react'
import {  
  Input,
  Form
} from 'semantic-ui-react'
import './styles/InputTextStyle.css'
const screenW = 1024;
const screenH = 768;
const ratioH = 768/window.innerHeight;
const ratioW = 1024/ window.iinnerWidth;

const InputText = (props) => {

    return (
        <Form.Field style= {{marginBottom: 40, color: '#fff'}}>
          <Input 
              transparent 
              className="inputText"  
              placeholder= {props.placeholder} 
              type= {props.type}
              {...props}
              >
          </Input>
        </Form.Field>
    );

}
export default InputText;