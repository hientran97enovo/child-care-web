import React, { Component } from 'react'
import {  
  Button, Icon,
  Grid, 
} from 'semantic-ui-react'
import './styles/InputTextStyle.css'

const screenW = 1024;
const screenH = 768;
const ratioH = 768/window.innerHeight;
const ratioW = 1024/ window.iinnerWidth;

const SocialLoginButton = (props) => {
    return (
        <Grid divided='vertically'>
            <Grid.Row 
            columns={3} >
            <Grid.Column largeScreen ={3} mobile = {1} computer= {2} tablet = {3} >
            </Grid.Column>
                <Grid.Column largeScreen ={5} mobile = {7} computer= {6} tablet = {5} >
                    <Button.Group className="socialButton">
                        <Button color='facebook'>
                            <Icon name='facebook' /> Facebook
                        </Button>
                        <Button.Or />
                        <Button color='google plus'>
                            <Icon name='google plus' /> Google
                        </Button>
                    </Button.Group>
                </Grid.Column >
            <Grid.Column largeScreen ={3} mobile = {1} computer= {2} tablet = {3}>
            </Grid.Column>
            </Grid.Row>
        </Grid>
    );
}

export default SocialLoginButton;