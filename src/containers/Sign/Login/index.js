import React, { Component } from 'react'
import { 
  Button, Checkbox, 
  Form, Grid,
  Segment, Divider,
  Icon
} from 'semantic-ui-react'
import './style.css'
import InputText from '../components/InputText';
import SocialLoginButton from '../components/SocialLoginButton';
import LoginActions from '../../../redux/LoginRedux/actions';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import store from '../../../redux/store'

const screenW = 1024;
const screenH = 768;
const ratioH = 768/window.innerHeight;
const ratioW = 1024/ window.innerWidth;

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: '',
      password: '',
    }
    this.onChangeUserName = this.onChangeUserName.bind(this);
    this.onChangePassword = this.onChangePassword.bind(this);
  }
  componentWillUpdate (nextProps) {
    if (nextProps.isLogined)
      store.dispatch(push('/'));
  }


  onChange(data) {
    this.setState({ ...data });
  }

  onChangeUserName(text) {
    this.setState({ username: text.target.value });
  }

  onChangePassword(text) {
    this.setState({ password: text.target.value });
  }

  onChangeLoadingState(isloading) {
     this.setState({ isloading: isloading});
  }

  onLogin(){
    const data = {
      username: this.state.username,
      password: this.state.password,
    };
    console.log(this.state.username);
    this.props.login(data);
    console.log(this.props.login(data))
  }

  render() {
    return (
      <Grid.Column largeScreen ={5} mobile = {12} computer= {5} tablet = {5} className='sign-form' style={{ marginTop: 200* ratioH, paddingTop: 100*ratioH}}>
        <h1 className="header-form">Login</h1>
        <Form 
          className= 'formMargin' 
          style={{marginTop: 50*ratioH}} 
          onSubmit={()=>this.onLogin()}>
          <InputText 
            placeholder = 'Email or username'
            onChange = {this.onChangeUserName} 
            value={this.state.username}
          />
          <InputText 
            placeholder = 'Password' 
            type = 'password'
            onChange = {this.onChangePassword} 
            value={this.state.password}
          />
          <Button type='submit' color = "orange" className="button" onClick={()=>this.onLogin()}>
            Login
          </Button>
          <div className= 'divider'>
            <Divider inverted />
            <Divider horizontal inverted>OR</Divider>
          </div> 
          <div className= 'socialButtonDiv'>
          <SocialLoginButton />
          </div>
        </Form>
      </Grid.Column>
    );
  }
}

function mapStateToProps(state) {
  return {
    isLogined: state.user.isLogined,
    data: state.user.data,
    error: state.login.error,
    isloading: state.login.loading,
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    login: data => dispatch(LoginActions.login(data)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
