import React, { Component } from 'react'
import { 
  Button, Checkbox, 
  Form, Grid,
  Segment, Divider,
  Icon
} from 'semantic-ui-react'
import './style.css'
import InputText from '../components/InputText';
import SocialLoginButton from '../components/SocialLoginButton';
const screenW = 1024;
const screenH = 768;
const ratioH = 768/window.innerHeight;
const ratioW = 1024/ window.innerWidth;
class Register extends Component {
  render() {
    return (
      <Grid.Column floated='left' width={5} className='sign-form' style={{ marginTop: 200* ratioH, paddingTop: 100*ratioH}}>
        <h1 className="header-form">Login</h1>
        <Form className= 'formMargin' style={{marginTop: 50*ratioH}}>
          <InputText placeholder = 'Email or username'/>
          <InputText placeholder = 'Password' type = 'password'/>
          <Button type='submit' color = "orange" className="button">
            Login
          </Button>
          <div className= 'divider'>
            <Divider inverted />
            <Divider horizontal inverted>OR</Divider>
          </div> 
          <div className= 'socialButtonDiv'>
          <SocialLoginButton />
          </div>
        </Form>
      </Grid.Column>
    );
  }
}

export default Register;
