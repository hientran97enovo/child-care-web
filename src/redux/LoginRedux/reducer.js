import Immutable from 'seamless-immutable';
import { LoginTypes } from './actions';
import { createReducer } from '../../utils/ReduxUtils';

export const INITIAL_STATE = Immutable({
    username: '',
    password: '',
    email : '',
    loading: false,
    error: null,
    data: null,
});

export const login = (state, action) => 
    state.merge({ loading: true, error: null, data: null });

export const loginSuccess = (state, { response }) =>
    state.merge({ loading: false, data: response });

export const loginFailure = (state, { err }) => 
    state.merge({ loading: false, error: err });

const reducer = createReducer(INITIAL_STATE, {
    [LoginTypes.USER_LOGIN]: login,
    [LoginTypes.USER_LOGIN_FB]: login,
    [LoginTypes.USER_LOGIN_GMAIL]: login,
    [LoginTypes.USER_LOGIN_SUCCESS]: loginSuccess,
    [LoginTypes.USER_LOGIN_FAILURE]: loginFailure
});

export default reducer;