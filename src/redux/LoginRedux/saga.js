import { put, call, select } from 'redux-saga/effects';
import UserActions from './actions';
import Api from '../../services/ParseApi';

export function* login({ data }) {
  try {
    const ParseApi = new Api(null);
    const response = yield call([ParseApi, ParseApi.login], data);
    if (response && response.error) {
      yield put(UserActions.loginFailure(response));
      console.log(response.error);
      console.log(response);
    } else {
      console.log(data);
      yield put(UserActions.loginSuccess(response));
    }
  } catch (error) {
    yield put(UserActions.loginFailure(error));
  }
}

export function* logout() {
  try {
    const { sessionToken } = yield select(state => state.user.data);
    const ParseApi = new Api(sessionToken);
    yield call([ParseApi, ParseApi.logout]);
  } catch (error) {
    yield put(UserActions.loginFailure(error));
  }
}

