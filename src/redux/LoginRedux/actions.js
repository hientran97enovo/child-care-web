import { createAction } from '../../utils/ReduxUtils';

export const LoginTypes = {
    USER_LOGIN: 'USER_LOGIN',
    USER_LOGIN_FB: 'USER_LOGIN_FB',
    USER_LOGIN_GMAIL: 'USER_LOGIN_GMAIL',
    USER_LOGIN_SUCCESS: 'USER_LOGIN_SUCCESS',
    USER_LOGIN_FAILURE: 'USER_LOGIN_FAILURE',
    USER_LOGOUT: 'USER_LOGOUT',
};

const login = data => createAction(LoginTypes.USER_LOGIN, { data });
const loginFB = data => createAction(LoginTypes.USER_LOGIN_FB, { data });
const loginGmail = data => createAction(LoginTypes.USER_LOGIN_GMAIL, { data });
const loginSuccess = response => createAction(LoginTypes.USER_LOGIN_SUCCESS, { response });
const loginFailure = err => createAction(LoginTypes.USER_LOGIN_FAILURE, { err });
const logout = data => createAction(LoginTypes.USER_LOGOUT, { data });

export default {
  login,
  loginFB,
  loginGmail,
  loginSuccess,
  loginFailure,
  logout,
};
