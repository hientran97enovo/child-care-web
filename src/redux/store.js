import { createStore, applyMiddleware, compose } from 'redux'
import { routerMiddleware } from 'react-router-redux'
import createSagaMiddleware from 'redux-saga';
import { autoRehydrate } from 'redux-persist';
import thunk from 'redux-thunk'
import createHistory from 'history/createBrowserHistory'
import reducers from './reducers';
import R from 'ramda';
import rootReducer from './reducers';
import rootSaga from './sagas';


export const history = createHistory()

const initialState = {}
const enhancers = []
const middleware = [
  thunk,
  routerMiddleware(history)
]

if (process.env.NODE_ENV === 'development') {
  const devToolsExtension = window.devToolsExtension

  if (typeof devToolsExtension === 'function') {
    enhancers.push(devToolsExtension());
     enhancers.push(applyMiddleware(...middleware));
  }
}
  /* ------------- Logger Middleware ------------- */

  const SAGA_LOGGING_BLACKLIST = ['EFFECT_TRIGGERED', 'EFFECT_RESOLVED', 'EFFECT_REJECTED', 'persist/REHYDRATE'];
/* ------------- Saga Middleware ------------- */

const sagaMiddleware = createSagaMiddleware();
  middleware.push(sagaMiddleware);


const composedEnhancers = compose(
  applyMiddleware(...middleware),
  ...enhancers
)

const store = createStore(
  reducers,
  initialState,
  composedEnhancers
)

// kick off root saga
sagaMiddleware.run(rootSaga);

export default store
