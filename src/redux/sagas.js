import { takeLatest } from 'redux-saga/effects';
/* ------------- Types ------------- */

import { LoginTypes } from './LoginRedux/actions';
import { ClassTypes } from './ClassRedux/actions';

/* ------------- Sagas ------------- */

import { login, logout } from './LoginRedux/saga';
import { getClassData } from './ClassRedux/saga';


/* ------------- Connect Types To Sagas ------------- */
export default function* root() {
  yield [
    // some sagas only receive an action
    takeLatest(LoginTypes.USER_LOGIN, login),
    takeLatest(LoginTypes.USER_LOGOUT, logout),
    takeLatest(ClassTypes.GET_CLASS_DATA, getClassData),
  ];
}
