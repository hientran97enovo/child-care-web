import Immutable from 'seamless-immutable';
import { LoginTypes } from '../LoginRedux/actions';
import { createReducer } from '../../utils/ReduxUtils';

export const INITIAL_STATE = Immutable({
  isLogined: false,
  data: null,
  error: null,
  userData: null,
  sessionToken: null,
});

export const loginSuccess = (state, { response }) =>
  state.merge({ isLogined: true, userData: response, sessionToken: response.sessionToken  });

export const logout = state =>
  state.merge({ data: {}, isLogined: false });

const reducer = createReducer(INITIAL_STATE, {
  [LoginTypes.USER_LOGIN_SUCCESS]: loginSuccess,
  [LoginTypes.USER_LOGOUT]: logout,
});

export default reducer;