import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux'
import LoginReducer from './LoginRedux/reducer';
import UserReducer from './UserRedux/reducer';
import ClassReducer from './ClassRedux/reducer';


export default combineReducers({
  router: routerReducer,
  login: LoginReducer,
  user: UserReducer,
  class: ClassReducer,
});
