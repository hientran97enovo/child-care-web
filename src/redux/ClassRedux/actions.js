import { createAction } from '../../utils/ReduxUtils';


export const ClassTypes = {
    GET_CLASS_DATA: 'GET_CLASS_DATA',
    GET_CLASS_DATA_SUCCESS: 'GET_CLASS_DATA_SUCCESS',
    GET_CLASS_DATA_FAILURE: 'GET_CLASS_DATA_FAILURE',
};
const getClassData= data => createAction(ClassTypes.GET_CLASS_DATA, { data });
const getClassDataSuccess= response => createAction(ClassTypes.GET_CLASS_DATA_SUCCESS, {response});
const getClassDataFailure = err => createAction(ClassTypes.GET_CLASS_DATA_FAILURE, { err });

export default {
    getClassData,
    getClassDataSuccess,
    getClassDataFailure
}