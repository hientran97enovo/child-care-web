import Api from '../../services/ParseApi';
import ClassActions from './actions';
import { put, call, select } from 'redux-saga/effects';

export function* getClassData() {
    try {
    const ParseApi = new Api(null);
    const response = yield call([ParseApi, ParseApi.getClassData]);
    
    if (response && response.error) {
      yield put(ClassActions.getClassDataFailure(response));
    } else {
      yield put(ClassActions.getClassDataSuccess(response.results));
    }
  } catch (error) {
    yield put(ClassActions.getClassDataFailure(error));
  }
}