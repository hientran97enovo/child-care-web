import Immutable from 'seamless-immutable';
import { ClassTypes } from './actions';
import { createReducer } from '../../utils/ReduxUtils';

export const INITIAL_STATE = Immutable({
    loading: false,
    data: null,
    error: null,
});

export const getClassData = state => 
    state.merge({ error: null, loading: true });

export const getClassDataSuccess = (state, { response }) => 
    state.merge({ loading: false, data: response });


export const getClassDataFailure = (state, { err }) => 
    state.merge({ loading: false, error: err });

const reducer = createReducer(INITIAL_STATE, {
    [ClassTypes.GET_CLASS_DATA]: getClassData,
    [ClassTypes.GET_CLASS_DATA_SUCCESS]: getClassDataSuccess,
    [ClassTypes.GET_CLASS_DATA_FAILURE]: getClassDataFailure
});

export default reducer;