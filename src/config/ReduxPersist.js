import { AsyncStorage } from 'AsyncStorage';
import immutablePersistenceTransform from '../utils/ImmutablePersistenceTransform';

const REDUX_PERSIST = {
  active: true,
  reducerVersion: '2',
  storeConfig: {
    whitelist: ['user'],
    storage: AsyncStorage,
    transforms: [immutablePersistenceTransform],
  },
};

export default REDUX_PERSIST;

