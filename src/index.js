import React from 'react';
import ReactDOM from 'react-dom';
import { Route, Switch } from 'react-router-dom';
import { ConnectedRouter } from 'react-router-redux';
import Loadable from 'react-loadable';
import 'semantic-ui-css/semantic.min.css';
import './styles/index.css';
import App from './App';
import {Provider} from 'react-redux';
import store, { history } from './redux/store';
import Loading from './components/common/LoadingScreen';


const target = document.querySelector('#root');
const AsyncHome = Loadable({
  loader: () => import('./containers/Home'),
  loading: Loading,
});
const AsyncLogin = Loadable({
  loader: () => import('./containers/Sign/Login'),
  loading: Loading,
});
const AsyncClass = Loadable({
  loader: () => import('./containers/Class'),
  loading: Loading,
});
ReactDOM.render(
<Provider store={store}>
    <ConnectedRouter history={history}>
      <Switch>
        <Route exact path="/" component={App} />
         <Route path="/login" component={AsyncLogin} />
          <Route path="/class" component={AsyncClass} />
      </Switch>
    </ConnectedRouter>
  </Provider>
    ,target);

